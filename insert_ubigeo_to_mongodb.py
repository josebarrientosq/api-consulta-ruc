# -*- coding: utf-8 -*-
import io
import os
import json
import pymongo
from pymongo import MongoClient


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
db = client.tododb
"""
CODIGO|NOMBRE_UBIGEO|
"""

db.ubigeo.create_index([('codigo', pymongo.ASCENDING)],unique=True)

txt = open(u"RCD-UBIGEO.txt", "r")

r = True
cnt = 0
r = txt.readline()
while r:
    s = r.split("|")
    record = {}
    record["codigo"] = s[0]
    record["nombre_ubigeo"] = s[1]

    try:
        r = txt.readline()
        db.ubigeo.insert_one(record)
        print(cnt)

    except:
        pass
    cnt += 1


txt.close()

