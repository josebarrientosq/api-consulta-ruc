# -*- coding: utf-8 -*-
import io
import os
import json
import pymongo
from pymongo import MongoClient


client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
db = client.tododb


db.empresa.create_index([('ruc', pymongo.ASCENDING)],unique=True)
print("indexado por ruc")

txt = open(u"padron_reducido_ruc.txt", "r")

r = True
cnt = 0
r = txt.readline()
while r:
    s = r.split("|")
    record = {}
    record["ruc"] = s[0]
    record["nombre_o_razon_social"] = s[1]
    record["estado_del_contribuyente"] = s[2]
    record["condicion_de_domicilio"] = s[3]
    record["ubigeo"] = s[4]
    record["tipo_de_via"] = s[5]
    record["nombre_de_via"] = s[6]
    record["codigo_de_zona"] = s[7]
    record["tipo_de_zona"] = s[8]
    record["numero"] = s[9]
    record["interior"] = s[10]
    record["lote"] = s[11]
    record["dpto"] = s[12]
    record["manzana"] = s[13]
    record["kilometro"] = s[14]

    try:
        r = txt.readline()
        db.empresa.insert(record)
        print(cnt)
    except:
        pass
    cnt += 1


txt.close()

