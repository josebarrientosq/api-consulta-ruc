import io
import os
import json
from flask import Flask, redirect, url_for, request, render_template,jsonify
from pymongo import MongoClient


app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
#client = MongoClient('localhost:27017')
db = client.tododb


@app.route('/')
def todo():

    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)


@app.route('/new', methods=['POST'])
def new():

    item_doc = {
        'ruc': request.form['ruc'],
        'nombre': request.form['nombre']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

@app.route('/user', methods=['GET'])
def user():
    ''' route read user '''
    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)



@app.route('/addruc', methods=['post'])
def add_ruc():
    empresa = db.tododb
    ruc= request.json['ruc']
    nombre = request.json['nombre']

    s = empresa.find_one({'ruc': ruc})
    if s:
        output = "Ya existe el ruc"
    else:
        empresa_id = empresa.insert({'ruc': ruc, 'nombre': nombre})
        output = "agregado"
    return jsonify({'result': output})



@app.route('/empresa/<ruc>', methods=['GET'])
def get_one_empresa(ruc):
    empresa = db.tododb
    s = empresa.find_one({'ruc': ruc})
    if s:
        output = {'ruc': s['ruc'], 'nombre': s['nombre']}
    else:
        output = "No se encontro"
    return jsonify({'result': output})

@app.route('/delete', methods=['GET'])
def deleteall():
    db.tododb.remove({})
    return "deleted"


@app.route('cargar',methods=['GET'])
def cargar():




if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)