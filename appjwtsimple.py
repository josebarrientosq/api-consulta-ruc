import io
import os
import json
from flask import Flask, redirect, url_for, request, render_template,jsonify
from pymongo import MongoClient

#from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp
from flask_jwt_simple import ( JWTManager, jwt_required, create_jwt, get_jwt_identity )


app = Flask(__name__)
app.debug = True
app.config['JWT_SECRET_KEY'] = 'B1Admin'

jwt = JWTManager(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
#client = MongoClient('localhost:27017')
db = client.tododb

@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    user = db.user.find_one({'username': username})

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    if user and safe_str_cmp(user["password"].encode('utf-8'), password.encode('utf-8')):
        ret = {'jwt': create_jwt(identity=username)}
        return jsonify(ret), 200
    else:
        return jsonify({"msg": "Bad username or password"}), 401

# Identity can be any data that is json serializable ret = {'jwt': create_jwt(identity=username)}



@app.route('/')
def todo():

    _items = db.empresa.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)


@app.route('/new', methods=['POST'])
def new():

    item_doc = {
        'ruc': request.form['ruc'],
        'nombre': request.form['nombre']
    }
    db.empresa.insert_one(item_doc)

    return redirect(url_for('todo'))

""""
@app.route('/users', methods=['GET'])
def users(ruc):
    s = db.user.find_one()
    if s:
        output = {  "success" : True,
                    "ruc": s["ruc"],
                    "nombre": s["nombre"],

                  }
    else:
        output = {  "success" : False}

    return jsonify(output)
"""
@app.route('/users/<username>', methods=['GET'])
def users(username):
    s = db.user.find_one({'username': username})
    if s:
        output = {  "success" : True,
                    "username": s["username"],
                    "password": s["password"],

                  }
    else:
        output = {  "success" : False}

    return jsonify(output)



@app.route('/add-user', methods=['POST'])
def add_user():
    username = request.json['username']
    password = request.json['password']

    db.user.insert({'username': username, 'password': password})

    s = db.user.find_one({'username': username})
    if s:
        output = {"success": True,
                  "username": s["username"],
                  "password": s["password"],

                  }
    else:
        output = {"success": False}

    return jsonify(output)



@app.route('/empresa/<ruc>', methods=['GET'])
def get_one_empresa(ruc):
    s = db.empresa.find_one({'ruc': ruc})
    if s:
        output = {  "success" : True,
                    "ruc": s["ruc"],
                    "nombre": s["nombre_o_razon_social"],
                    "estado_del_contribuyente" : s["estado_del_contribuyente"],
                    "condicion_de_domicilio" : s["condicion_de_domicilio"],
                    "ubigeo" : s["ubigeo"],
                    "tipo_de_via": s["tipo_de_via"],
                    "nombre_de_via" : s["nombre_de_via"],
                    "codigo_de_zona": s["codigo_de_zona"],
                    "tipo_de_zona" : s["tipo_de_zona"],
                    "numero" :s["numero"],
                    "interior" :s["interior"],
                    "lote" : s["lote"],
                    "dpto" : s["dpto"],
                    "manzana" : s["manzana"],
                    "kilometro" : s["kilometro"],
                    }
    else:
        output = {  "success" : False}

    return jsonify(output)


@app.route('/delete', methods=['GET'])
@jwt_required
def deleteall():
    db.empresa.remove({})
    return "deleted"

@app.route('/delete-users', methods=['GET'])
def deleteallusers():
    db.user.remove({})
    return "deleted"


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)