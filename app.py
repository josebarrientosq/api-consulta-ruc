import io
import os
import json
from flask import Flask, redirect, url_for, request, render_template,jsonify
from pymongo import MongoClient

#from flask_jwt import JWT, jwt_required, current_identity
from werkzeug.security import safe_str_cmp

from functools import wraps
from flask_jwt_extended import (JWTManager, jwt_required, create_access_token,get_jwt_identity,  get_jwt_claims, verify_jwt_in_request
)


app = Flask(__name__)
app.debug = True
app.config['JWT_SECRET_KEY'] = 'B1Admin'
app.config['JWT_ACCESS_TOKEN_EXPIRES']= False

jwt = JWTManager(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
#client = MongoClient('localhost:27017')
db = client.tododb

s = db.user.find({'username': 'admin'}).count()
if s==0:
    db.user.insert({'username': 'admin', 'password': 'admin', 'roles': 'admin'})

@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    user = db.user.find_one({'username': username})

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    if user and safe_str_cmp(user["password"].encode('utf-8'), password.encode('utf-8')):
        # Identity can be any data that is json serializable
        access_token = create_access_token(identity=user)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({"msg": "Bad username or password"}), 401

#chapter 5
@jwt.user_claims_loader
def add_claims_to_access_token(user):
    return {'roles': user["roles"]}

@jwt.user_identity_loader
def user_identity_lookup(user):
    return user["username"]

#chapter 7
def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if claims['roles'] != 'admin':
            return jsonify(msg='Admins only!'), 403
        else:
            return fn(*args, **kwargs)
    return wrapper

def esp(s):
    if (s=="-"):
        return ""
    else :
        return s+" "
def nro(s):
    if (s=="-"):
        return ""
    else :
        return "NRO. "+s+" "
def inte(s):
    if (s == "-"):
        return ""
    else:
        return "INT. "+ s + " "
def dpto(s):
    if (s == "-"):
        return ""
    else:
        return "DPTO. "+ s + " "
def mza(s):
    if (s == "-"):
        return ""
    else:
        return "MZA. "+ s + " "
def lote(s):
    if (s == "-"):
        return ""
    else:
        return "LOTE. "+ s + " "
def km(s):
    if (s == "-"):
        return ""
    else:
        return "KM. "+ s + " "

@app.route('/empresa', methods=['POST'])
@jwt_required
def get_one_empresa():
    ruc = request.json['ruc']
    s = db.empresa.find_one({'ruc': ruc})

    if s:
        distrito = db.ubigeo.find_one({'codigo': s["ubigeo"]})
        provincia= db.ubigeo.find_one({'codigo': s["ubigeo"][:4]+"00"})
        departamento= db.ubigeo.find_one({'codigo': s["ubigeo"][:2]+"0000"})

        output = {  "success" : True,
                    "ruc": s["ruc"],
                    "nombre": s["nombre_o_razon_social"],
                    "estado_del_contribuyente" : s["estado_del_contribuyente"],
                    "condicion_de_domicilio" : s["condicion_de_domicilio"],
                    "ubigeo" : s["ubigeo"],
                    "tipo_de_via": s["tipo_de_via"],
                    "nombre_de_via" : s["nombre_de_via"],
                    "codigo_de_zona": s["codigo_de_zona"],
                    "tipo_de_zona" : s["tipo_de_zona"],
                    "numero" :s["numero"],
                    "interior" :s["interior"],
                    "lote" : s["lote"],
                    "dpto" : s["dpto"],
                    "manzana" : s["manzana"],
                    "kilometro" : s["kilometro"],
                    "distrito" : distrito["nombre_ubigeo"],
                    "provicia": provincia["nombre_ubigeo"],
                    "departamento": departamento["nombre_ubigeo"],
                    "direccion_completa" : esp(s["tipo_de_via"])+esp(s["nombre_de_via"])+nro(s["numero"])+inte(s["interior"])+dpto(s["dpto"])+km(s["kilometro"])+mza(s["manzana"])+lote(s["lote"])+esp(s["codigo_de_zona"])+esp(s["tipo_de_zona"])+departamento["nombre_ubigeo"]+" "+provincia["nombre_ubigeo"]+" "+distrito["nombre_ubigeo"]
                    }
    else:
        output = {  "success" : False,
                    "msg": "No se encuentra el ruc"}

    return jsonify(output)


@app.route('/')
@admin_required
def todo():

    _items = db.empresa.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)


@app.route('/new', methods=['POST'])
@admin_required
def new():

    item_doc = {
        'ruc': request.form['ruc'],
        'nombre': request.form['nombre']
    }
    db.empresa.insert_one(item_doc)

    return redirect(url_for('todo'))

""""
@app.route('/users', methods=['GET'])
def users(ruc):
    s = db.user.find_one()
    if s:
        output = {  "success" : True,
                    "ruc": s["ruc"],
                    "nombre": s["nombre"],

                  }
    else:
        output = {  "success" : False}

    return jsonify(output)
"""
@app.route('/get-one-user', methods=['POST'])
@admin_required
def get_one_user():
    username = request.json['username']
    s = db.user.find_one({'username': username})
    if s:
        output = {  "success" : True,
                    "username": s["username"],
                    "password": s["password"],
                    "roles": s["roles"]
                  }
    else:
        output = {  "success" : False}

    return jsonify(output)

@app.route('/get-all-user', methods=['POST'])
@admin_required
def get_all_user():
    users = db.user.find({})
    output=[]

    for user in users:
        output.append({
            "username" : user["username"],
            "password" : user["password"]
        })

    return jsonify(output)

@app.route('/add-one-user', methods=['POST'])
@admin_required
def add_one_user():
    username = request.json['username']
    password = request.json['password']
    roles = request.json['roles']

    s = db.user.find({'username': username}).count()

    if s==0:
        db.user.insert({'username': username, 'password': password, 'roles': roles})
        output = {"success": True,
                  "username": username,
                  "password": password,
                  "roles": roles,
                  "msg" : "Se inserto 1 usuario"

                  }
    else:
        output = {"success": False,
                  "msg" : "ya existen "+str(s)+" registro"}

    return jsonify(output)

@app.route('/set-password-user', methods=['POST'])
@admin_required
def set_password_user():
    username = request.json['username']
    newpassword = request.json['newpassword']

    s = db.user.update_one({'username': username},{"$set": {"password": newpassword}} )
    if s.modified_count==1:
        output = {"success": True,
                  "msg": "Se cambio el password"
                 }
    else:
        output = {"success": False,
                  "msg": "No se modifico"}

    return jsonify(output)


@app.route('/delete-all-empresa', methods=['GET'])
@admin_required
def deleteallempresa():
    db.empresa.remove({})
    ret = {
            'current_identity': get_jwt_identity(),
            'current_roles': get_jwt_claims(),
    }
    return jsonify(ret), 200

@app.route('/delete-all-user', methods=['GET'])
@admin_required
def deleteallusers():
    db.user.remove({})
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user ), 200

@app.route('/delete-one-user', methods=['POST'])
@admin_required
def deleteoneuser():
    username = request.json['username']
    s=db.user.delete_one({'username': username})
    if s.deleted_count==1:
        output = {"success": True,
                  "data" : s.deleted_count,
                  "msg": "Se borro el usuario "+ username
                 }
    else:
        output = {"success": False,
                  "msg": "Se borraron " +str(s.deleted_count)+" registros"}

    return jsonify(output)

@app.route('/get-ubigeo', methods=['POST'])
@admin_required
def get_ubigeo():
    codigo = request.json['codigo']

    s = db.ubigeo.find_one({'codigo': codigo})
    output = {"codigo" : s["codigo"],
              "ubigeo" : s["nombre_ubigeo"]}
    return jsonify(output)

@app.route('/get-all-ubigeo', methods=['POST'])
@admin_required
def get_all_ubigeo():
    ubigeos = db.ubigeo.find({})
    output=[]

    for ubigeo in ubigeos:
        output.append({
            "codigo" : ubigeo["codigo"],
            "ubigeo" : ubigeo["nombre_ubigeo"]
        })

    return jsonify(output)

@app.route('/delete-all-ubigeo', methods=['POST'])
@admin_required
def deleteallubigeo():
    db.ubigeo.remove({})
    return "deletedubigeo"
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)