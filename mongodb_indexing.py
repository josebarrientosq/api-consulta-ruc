import os
import pymongo
from pymongo import MongoClient

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
db = client.tododb

empresa = db.tododb

empresa.create_index([('ruc', pymongo.ASCENDING)],unique=True)
print("indexado por ruc")