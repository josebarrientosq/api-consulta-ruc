FROM python:2.7
ADD . /todo
WORKDIR /todo
USER root
RUN set -x; \
	apt-get update
RUN apt-get install unzip
RUN pip install -r requirements.txt
RUN apt-get install cron
COPY ./cron /etc/cron.daily
